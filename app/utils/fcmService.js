var admin = require("firebase-admin");
var logger = require('winston');

module.exports = {
    sendMessage:function(fcmToken, title, body) {
        if (fcmToken) {
            logger.debug("FCMTOKEN found sending message:" + title);

            var payload = {
                notification: {
                    title: title,
                    body: body,
                    icon: "ic_notif"
                }
            };

            admin.messaging().sendToDevice(fcmToken,
                    payload)
                .then(function (response) {
                    // See the MessagingDevicesResponse reference documentation for
                    // the contents of response.
                    logger.debug("Successfully sent fcm challenge message");
                })
                .catch(function (error) {
                    logger.error("Error sending fcm challenge message:", error);
                });
        } else {
            logger.error("USER WITHOUT FCM token");
        }

    }
}
