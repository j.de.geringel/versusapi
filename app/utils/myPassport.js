var passport = require('passport')
var logger = require('winston');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
var FacebookStrategy = require('passport-facebook').Strategy;

var User = require('./../user/User');



// Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
passport.use(
  new GoogleStrategy({
      clientID: '511812878306-v56oai9sth0bue6cd2tjmt56d29iav7p.apps.googleusercontent.com',
      clientSecret: 'zU3JCOv1oyYEfU_tSqu91IE_',
      callbackURL: "https://api.polyprog.io/api/user/login/google/callback",
      passReqToCallback: true
    },
    function (req, accessToken, refreshToken, profile, done) {
      //profile = JSON.parse(profile);
      logger.debug("Got token from google, finding or creating user..");
      User.findOrCreate({
          socialId: profile.id,
          fcmToken: req.query ? req.query.state : null,
          type: 'g',
          username: profile.name.givenName ? profile.name.givenName : profile.displayName,
          email: profile.emails ? profile.emails[0] : null,
          image: profile.photos ? profile.photos[0].value.replace(sz = 50, sz = 75) : null
        })
        .then(function (user) {
          logger.debug("Google user found or inserted, returing done in strategy");
          return done(null, user);
        })
        .catch(function (err) {
          logger.error("Error when finding or creating Google user:" + err);
          return done(err, null);
        });
    }
  ));

passport.use(new FacebookStrategy({
    clientID: "167993033701534",
    clientSecret: "3a514c1a011d46bd42ae9cd4a2df3302",
    callbackURL: "https://api.polyprog.io/api/user/login/facebook/callback",
    profileFields: ['id', 'displayName', 'name', 'photos'],
    passReqToCallback: true

  },
  function (req, accessToken, refreshToken, profile, done) {
     logger.debug('profile:' + JSON.stringify(profile));
     User.findOrCreate({
          socialId: profile.id,
          fcmToken: req.query ? req.query.state : null,
          type: 'f',
          username: profile.name.givenName ? profile.name.givenName : profile.displayName.split(" ")[0],
          email: profile.emails ? profile.emails[0] : null,
          image: profile.photos ? profile.photos[0].value: null
        })
        .then(function (user) {
          logger.debug("Facebook user found or inserted, returing done in strategy");
          return done(null, user);
        })
        .catch(function (err) {
          logger.error("Error when finding or creating Google user:" + err);
          return done(err, null);
        });

  }
));

module.exports = passport;