"use strict";


var logger = require('winston');

logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    timestamp: function () {
        return getCurrentTime();
    }, level: 'debug', colorize: true
});


function getCurrentTime()
{
    let date = new Date();
    return date.getMonth() +1 +'-' + date.getDate() + ' ' + date.getHours() + ':'+ date.getMinutes() + '.'+ date.getSeconds();
}