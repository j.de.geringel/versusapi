"use strict";

var db = require('./../../db').get();
var logger = require('winston');
var fcmService = require('./../utils/fcmService');

async function getLatestRoundResults(req, res, next) {

  let query;
  if (req.query.round) {
    query = "select * from (select GameUser.*, Users.image from GameUser, Users where GameUser.gameid = ($1) and Users.id = GameUser.userid) gu left join " +
      "(select points, round, stars, userid as resultuserid  from GameResult where gameid = ($1) and round=($3)) gr on gr.resultuserid = gu.userid ORDER BY gr.points";
  } else {
    query = "select * from (select GameUser.*, Users.image from GameUser, Users where GameUser.gameid = ($1) and Users.id = GameUser.userid) gu left join " +
      "(select points, round, stars, userid as resultuserid  from GameResult where gameid = ($1) and round=(select max(round) from GameResult where userid = ($2) and gameid= ($1))) gr on gr.resultuserid = gu.userid ORDER BY gr.points";
  }

  try {
    let data = await db.any(query, [req.query.gameid, req.query.userid, req.query.round]);
    res.status(200).json({
      status: 'success',
      data: data
    });

  } catch (error) {
    console.log(error);
    return next(error);
  };
}

async function lockResult(req, res, next) {
  try {
    await db.none("insert into gameresult (gameid ,userid, round, points, gametype) " +
      "values (${gameid}, ${userid}, ${round}, -1, ${gametype})", req.body);


    res.status(200).json({
      status: 'success'
    });

  } catch (error) {
    console.log(error);
    return next(error);
  };
}


async function createResult(req, res, next) {
  // first do simple checksum if not tamperd with
  if (req.headers['nginxts'] != (req.body.curTimeInMilis + (req.body.stars * 200) + req.body.points)) {
    logger.error("TEMPERED WITH, check:"+ req.headers['nginxts'] +", body: "+ JSON.stringify(req.body));
    logger.error("SHOULD BE:"+ (req.body.curTimeInMilis + (req.body.stars * 200) + req.body.points));
    res.status(400).json({ status: 'Bad request' });
  } else {
    // not tempered with
    try {
      await db.tx(async(t) => {
        return await t.batch([
          t.none("update gameuser set totalscore= COALESCE(totalscore, 0) + ${stars}, lastround = ${round} WHERE userid=${userid} and gameid=${gameid}", req.body),
          t.one("update gameresult set points = ${points}, stars = ${stars} where userid = ${userid} and gameid =${gameid} and round = ${round} and points = -1 RETURNING resultid", req.body)
        ]);
      });


      // logic below to set user and game end statusses
      if (req.body.round == req.body.maxNoRounds) { // last round
        let gameuserData = await db.one("update gameuser set usergamestatus=1 WHERE userid=${userid} and gameid=${gameid} returning opponentid, username", req.body);

        logger.debug("Last round detected, checking to set entire game as finished...");
        let noPlayersFinished = await db.one("select count(1) from gameuser WHERE gameid=${gameid} and lastround = ${maxNoRounds}", req.body);
        
        // look up fcm token for opponent
        var opponent = await db.one("select * from users WHERE id=$1", [gameuserData.opponentid]);
      
        if (noPlayersFinished.count == 2) { //finished
          logger.debug("Last player played round, seting game as finished...");
          let winner = await db.one("update game set gamestatus=9, winner=(select userid from gameuser where gameid = ${gameid} order by totalscore desc limit 1) WHERE gameid=${gameid} returning winner", req.body);
          logger.debug(winner.winner + " won the game");

          let fcmBody = gameuserData.opponentid == winner.winner ? 'You won the game!' : gameuserData.username + ' won the game!';
          fcmService.sendMessage(opponent.fcmtoken, "Versus Game Completed", fcmBody);
          
        }
        else { // player finished, notify opponent it's his turn
          logger.debug("Player finished, opponent has to play more rounds");
          fcmService.sendMessage(opponent.fcmtoken, gameuserData.username + " Is Waiting", gameuserData.username + ' finished all his rounds, your turn.');
        }
      }

      res.status(200).json({
        status: 'success',
        message: 'Inserted Result'
      });
    } catch (error) {
      console.log(error);
      return next(error);
    };
  }
}

module.exports = {
  createResult: createResult,
  lockResult: lockResult,
  getLatestRoundResults: getLatestRoundResults
};