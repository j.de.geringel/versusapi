var express = require('express')
var router = express.Router()
var logger = require('winston');

var qs = require('./Result');



router.get('/api/results', function (req, res, next) {
    //console.log(req);
    qs.getLatestRoundResults(req, res, next);
});

router.post('/api/result', function (req, res, next) {
    //console.log(req);
    qs.createResult(req, res, next);
});

router.post('/api/result/lock', function (req, res, next) {
    //console.log(req);
    qs.lockResult(req, res, next);
});




module.exports = router;