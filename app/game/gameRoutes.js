var express = require('express')
var router = express.Router()
var logger = require('winston');

var qs = require('./Game');

router.post('/api/game/random', async function (req, res, next) {
    try {
        await qs.joinOrStartRandomGame(req.body.userid, req.body.username);
        res.status(200)
            .json({
                status: 'success',
                message: 'Game created',
                data: {}
            });
    } catch (error) {
        console.log(error);
        return next(error);
    };
});

router.post('/api/game/accept', async function (req, res, next) {
    try {
        await qs.acceptChallenge(req.body.gameid);
        res.status(200)
            .json({
                status: 'success',
                message: 'Game accepted',
                data: {}
            });
    } catch (error) {
        console.log(error);
        return next(error);
    };
});

router.post('/api/game/decline', async function (req, res, next) {
    try {
        await qs.rejectChallenge(req.body.userid, req.body.gameid);
        res.status(200)
            .json({
                status: 'success',
                message: 'Game rejected',
                data: {}
            });
    } catch (error) {
        console.log(error);
        return next(error);
    };
});

router.delete('/api/game', async function (req, res, next) {
    try {
        await qs.removeGame(req.body.userid, req.body.gameid, req.body.opponentid);

        res.status(200).json({
            status: 'success',
            message: 'GameStatus Updated'
        });
    } catch (error) {
        console.log(error);
        return next(error);
    };
});

router.post('/api/game', function (req, res, next) {
    //console.log(req);
    qs.createGame(req, res, next);
});

router.post('/api/game/finised', function (req, res, next) {
    //console.log(req);
    qs.setGameFinished(req, res, next);
});

router.get('/api/games', function (req, res, next) {
    //console.log(req);
    qs.getGames(req, res, next);
});

module.exports = router;