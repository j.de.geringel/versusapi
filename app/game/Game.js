var logger = require('winston');

var db = require('./../../db').get();
var fcmService = require('./../utils/fcmService');


/** GAME_USER QUERIES */
var minimalAppVersion = 102;

var UPD_GAME_USER_WITH_CHALLENGER = "UPDATE GameUser SET usergamestatus=0, opponentname=$2, opponentid=$1, opponentimage = (SELECT image from users where id = $1) " +
    "WHERE gameuserid = (SELECT gameuserid FROM GameUser WHERE usergamestatus=7 " +
    "AND userid != $1 ORDER BY gameuserid LIMIT 1) returning *";

var INSERT_GAME_DEFAULT = "INSERT INTO Game DEFAULT VALUES returning gameid";

/** GAME QUERIES */
var INSERT_GAME_USER = "insert into GameUser(gameid, userid, opponentname, username, usergamestatus) values($1, $2, $3, $4, $5)";

async function joinOrStartRandomGame(useridParam, usernameParam) {

    logger.debug('Join or Starting random game, first check if joining is possible...');
    // updating 'opponent' row first to try and set this request as challenger
    let opponentGame = await db.any(UPD_GAME_USER_WITH_CHALLENGER, [useridParam, usernameParam]);

    if (opponentGame.length == 0) { // no random game available
        logger.debug('No open random game available, creating new one...');

        let gameid = await db.one(INSERT_GAME_DEFAULT);
        await db.none(INSERT_GAME_USER, [gameid.gameid, useridParam, 'Waiting for opponent to join...', usernameParam, 7]);

    } else {
        logger.debug('Open random game available, joining...');
        // , [gameid.gameid, req.body.organiser, req.body.organiserName, req.body.inviteeName, req.body.invitee]
        await db.none("insert into GameUser(gameid, userid, username, opponentname, opponentid, opponentimage) select $1, $2, $3, $4, $5, image FROM users where id = $5", [opponentGame[0].gameid, useridParam, usernameParam, opponentGame[0].username, opponentGame[0].userid]);
    }
}

async function acceptChallenge(gameid) {
    logger.debug("Challenge accepted for gameid:" + gameid + ', setting game active');
    await db.any("UPDATE GameUser SET usergamestatus=0 WHERE gameid = $1 ", [gameid]);

}

async function rejectChallenge(userid, gameid) {
    logger.debug("Rejecting challenge for :" + gameid);
    await db.tx(async(t) => {
        return await t.batch([
            t.none("UPDATE GameUser SET usergamestatus=9 WHERE gameid = $1 and userid= $2 and usergamestatus=6", [gameid, userid]),
            t.none("UPDATE GAME SET gamestatus=8 where gameid = $1", [gameid]) // entry for the one who's challenged
        ]);
    });
}


async function removeGame(userid, gameid, opponentid) {
    logger.debug("Removing game:" + gameid + ', for user:' + userid);
    let gameStatus = await db.one("SELECT gamestatus from game where gameid = $1", [gameid]);

    if (gameStatus.gamestatus !== 0) { // not an active game
        logger.debug("Not an acitve game removing from view for user");
        await db.any("UPDATE GameUser SET usergamestatus=9 WHERE userid=$1 and gameid = $2 ", [userid, gameid]);
    } else {
        let usergameStatus = await db.one("SELECT usergamestatus from GameUser where userid=$1 and gameid = $2", [userid, gameid]);
        logger.debug(usergameStatus.usergamestatus);
        if (usergameStatus.usergamestatus == 7 || usergameStatus.usergamestatus == 5) { // waiting for random or challenge accept
            logger.debug("Game not started yet removing from both views");
            await db.any("UPDATE GameUser SET usergamestatus=9 WHERE gameid = $2 ", [userid, gameid]);
        } else { // gameuserstatus 0 or 1
            logger.debug("Removing active game, setting opponent as winner");
            await db.tx(async(t) => {
                return await t.batch([
                    t.none("UPDATE GameUser SET usergamestatus=9 WHERE userid=$1 and gameid = $2 ", [userid, gameid]),
                    t.none("UPDATE GAME SET gamestatus=7, winner =$1 where gameid = $2", [opponentid, gameid]) // entry for the one who's challenged
                ]);
            });
        }
    }
}

async function createGame(req, res, next) {
    try {
        let sameGameActive = await db.one("select count(1) from GameUser where usergamestatus=6 and opponentid=$1 and userid=$2", [req.body.organiser, req.body.invitee]);
        console.warn(sameGameActive.count);
        if (sameGameActive.count == 0) {
            let gameid = await db.one("INSERT INTO Game DEFAULT VALUES returning gameid");

            await db.tx(async(t) => {
                return await t.batch([
                    t.none("insert into GameUser(gameid, userid, username, opponentname, opponentid, usergamestatus, opponentimage) select $1, $2, $3, $4, $5, $6, image FROM users where id = $5", [gameid.gameid, req.body.organiser, req.body.organiserName, req.body.inviteeName, req.body.invitee, 5]), // entry for the one who posted
                    t.none("insert into GameUser(gameid, userid, username, opponentname, opponentid, usergamestatus, opponentimage) select $1, $2, $3, $4, $5, $6, image FROM users where id = $5", [gameid.gameid, req.body.invitee, req.body.inviteeName, req.body.organiserName, req.body.organiser, 6]) // entry for the one who is challenged
                ]);
            });


            let user = await db.one("select * from users WHERE id=$1", [req.body.invitee]);
            fcmService.sendMessage(user.fcmtoken, req.body.organiserName + " Challenged You!", req.body.organiserName + " invited you to play a game of Versus!");
        }
        else {
            logger.debug("An exact same game is currently awaiting accept or reject, not starting a new one");
        }

        res.status(200).json({
            status: 'success',
            message: 'Inserted Game and Inv sent'
        });
    } catch (error) {
        console.log(error);
        return next(error);
    };
}

async function getGames(req, res, next) {
    try {
        var data;
        if (!req.query.version || req.query.version < minimalAppVersion) {
            logger.info("Out of date app detected: " + req.query.version);
            data = {
                shouldUpdate: true
            };
        } else {
            data = await db.any("select GameUser.*, game.*, users.username as winnername from GameUser, game LEFT JOIN users ON users.id=game.winner where Game.gameid = GameUser.gameid and userid = ($1)  and usergamestatus != 9", [req.query.userid]);
        }
        res.status(200).json({
            status: 'success',
            data: data
        });
    } catch (error) {
        logger.error(error);
        return next(error);
    };
}

async function setGameFinished(req, res, next) {

    try {
        await db.none("update game set gamestatus=9 WHERE gameid=${gameid}", req.body);

        res.status(200).json({
            status: 'success'
        });
    } catch (error) {
        console.log(error);
        return next(error);
    };
}

module.exports = {
    joinOrStartRandomGame: joinOrStartRandomGame,
    acceptChallenge: acceptChallenge,
    rejectChallenge,
    rejectChallenge,
    removeGame: removeGame,
    createGame: createGame,
    getGames: getGames,
    setGameFinished: setGameFinished
}