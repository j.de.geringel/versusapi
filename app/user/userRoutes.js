var express = require('express')
var router = express.Router()
var passport = require('passport')
var logger = require('winston');

var qs = require('./User');

router.post('/api/user', function (req, res, next) {
    logger.debug('Logging in or signing up:' + req.body.user.userName);
    qs.findUser(req.body.user.userName, req.body.user.password, req.body.user.fcmToken)
        .then(function (loginResult) {
            if (req.body.user.loginMode) {
                if (loginResult.userFound) {
                    logger.debug("Returning id:" + loginResult.id);
                    res.status(200)
                        .json({
                            status: 'success',
                            message: 'Logged in',
                            data: {
                                id: loginResult.id
                            }
                        });
                } else {
                    logger.debug('Not logged in:' + req.body.user.userName);
                    res.status(401)
                        .json({
                            status: 'Unauthorized',
                            message: "User not found with this password"
                        });
                }
            } else {
                logger.debug('Creating new user:' + req.body.user.userName);
                if (loginResult.userFound) {
                    logger.debug('Creating new user, but user with pw already exists:' + req.body.user.userName);
                    res.status(401)
                        .json({
                            status: 401,
                            message: 'This user already exist, did you mean to sign in?'
                        });

                } else {
                    qs.createUser(req.body.user)
                        .then(function (id) {
                            res.status(200)
                                .json({
                                    status: 'success',
                                    message: 'Inserted one User',
                                    data: id
                                });
                        })
                        .catch(function (err) {
                            logger.error(err);
                            return next(err);
                        });
                }
            }
        })
        .catch(function (err) {
            logger.error('Error finding user ' + req.body.user.userName + ', ' + err);
            return next(err);
        });
});

router.get('/api/user', function (req, res, next) {
    //console.log(req);
    qs.searchUsers(req, res, next);
});

router.post('/api/user/login', passport.authenticate('local', {
    session: false
}), function (req, res) {
    res.status(200)
        .json({
            status: 'success',
            message: 'Logged in!'
        });
});

router.get('/api/user/login/google/callback',
    passport.authenticate('google', {
        session: false,
        failureRedirect: '/socialError'
    }),
    function (req, res) {
        logger.debug('Google succes callback received for id:' + req.user.id + ', name:' + req.user.username);
        res.redirect('versus://home#username='+ req.user.username+'&userid='+ req.user.id);
    });


router.get('/api/intenttest', async function (req, res, next) {
   
    try {
        res.redirect('versus://home#username=Joris&userid=1234');
    } catch (error) {
        console.log(error);
        return next(error);
    };
});

router.get('/api/user/login/google', function (request, response) {
    logger.debug(request.get('User-Agent'));
    passport.authenticate('google', {
        scope: ['profile'],
        state: request.query.fcmtoken
    })(request, response);
});


router.get('/api/user/login/facebook/callback',
    passport.authenticate('facebook', {
        session: false,
        failureRedirect: '/socialError'
    }),
    function (req, res) {
        logger.debug('facebook succes callback received for id:' + req.user.id + ', name:' + req.user.username);
        res.redirect('versus://home#username='+ req.user.username+'&userid='+ req.user.id);
    })

router.get('/api/user/login/facebook', function (request, response) {
    passport.authenticate('facebook', {
        scope: ['public_profile'],
        state: request.query.fcmtoken
    })(request, response);
});



module.exports = router;