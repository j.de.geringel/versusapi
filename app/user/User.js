var db = require('./../../db').get();
var logger = require('winston');

// add query functions
function findOrCreate(socialUser) {
  return new Promise(function (resolve, reject) {
    if (socialUser.socialId) {
      db.oneOrNone("select * from users where socialid = ${socialId} ", socialUser)
        .then(function (user) {
          if (user) {
            if (!user.fcmtoken || user.fcmtoken != socialUser.fcmToken) {
              logger.debug("Updating user with new token");
              db.none("update users set fcmtoken=${fcmToken} WHERE socialid=${socialId}", socialUser);
            }
            logger.debug("Social user found with profile id, returning existing user");
            resolve(user);
          } else {
            logger.debug("Social user not found, creating a new one");
            db.one('insert into users(socialid, type, email, username, image, fcmtoken) values(${socialId}, ${type}, ${email}, ${username}, ${image}, ${fcmToken}) returning id', socialUser)
              .then(function (id) {
                resolve({
                  id: id.id,
                  username: socialUser.username
                });
              })
              .catch(function (err) {
                logger.error("Error when inserting new social user with profile: " + socialUser.socialId + ', err:' + err);
                reject(err);
              });
          }

        })
    } else {
      reject("No socialId found")
    };
  });
}


// add query functions
function createUser(userFromReq) {
  return db.one('insert into users(email, username, password, fcmtoken)' +
    'values(${email}, ${userName}, ${password}, ${fcmToken}) returning id', userFromReq);
}

// add query functions
function findUser(userName, password, fcmToken) {
  return new Promise(function (resolve, reject) {
    db.one('select * from users where username = ($1) and password = ($2)', [userName, password])
      .then(function (user) {
        if (!user.fcmtoken || user.fcmtoken != fcmToken) {
          logger.debug("Updating user with new token");
          logger.debug(user.fcmtoken);
          logger.debug(fcmToken);
          db.none("update users set fcmtoken=($1) WHERE id= ($2)", [fcmToken, user.id]);
        }

        logger.debug('User/password found in db');
        resolve({
          id: user.id,
          userFound: true
        });
      })
      .catch(function (err) {
        logger.debug("User/password not found: " + userName + password);
        resolve({
          userFound: false
        });
      });
  })
};

function searchUsers(req, res, next) {
  if (!req.query.search) {
    res.status(403).json({
      status: 'forbidden',
      message: 'Must provide query param'
    });
  } else {
    db.any("select id, username, image, timestamp from users where username ILIKE ${searchTerm}", {
        searchTerm: req.query.search + '%'
      })
      .then(function (data) {
        res.status(200)
          .json({
            status: 'success',
            data: data
          });
      })
      .catch(function (err) {
        return next(err);
      });
  }
}

module.exports = {
  findOrCreate: findOrCreate,
  createUser: createUser,
  findUser: findUser,
  searchUsers: searchUsers
}